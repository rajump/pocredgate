/*
    This script was generated by SQL Change Automation to help provide object-level history. This script should never be edited manually.
    For more information see: https://www.red-gate.com/sca/dev/offline-schema-model
*/

CREATE TABLE [dbo].[EMP_Table]
(
[emp_name] [nchar] (10) NULL,
[dept_name] [nchar] (10) NULL,
[ID] [varchar] (20) NOT NULL,
[devision] [nchar] (10) NULL,
[location] [nchar] (10) NULL,
[address] [nchar] (10) NULL,
[pin] [nchar] (10) NULL,
[address2] [nchar] (10) NULL,
[address3] [nchar] (10) NULL,
[add4] [nchar] (10) NULL,
[add5] [nchar] (10) NULL
)
GO
ALTER TABLE [dbo].[EMP_Table] ADD CONSTRAINT [PK__EMP_Tabl__3214EC2791DA53AC] PRIMARY KEY CLUSTERED ([ID])
GO
