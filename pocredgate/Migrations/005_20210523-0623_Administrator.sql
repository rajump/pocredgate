﻿-- <Migration ID="bc37afee-a9f1-41ab-8a7b-080f9a098337" />
GO

PRINT N'Altering [dbo].[EMP_Table]'
GO
ALTER TABLE [dbo].[EMP_Table] ADD
[address3] [nchar] (10) NULL
GO
ALTER TABLE [dbo].[EMP_Table] ALTER COLUMN [ID] [varchar] (20) NOT NULL
GO
PRINT N'Creating primary key [PK__EMP_Tabl__3214EC2791DA53AC] on [dbo].[EMP_Table]'
GO
ALTER TABLE [dbo].[EMP_Table] ADD CONSTRAINT [PK__EMP_Tabl__3214EC2791DA53AC] PRIMARY KEY CLUSTERED ([ID])
GO
