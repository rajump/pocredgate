﻿-- <Migration ID="4ea47a56-b600-43f2-9a35-988da55c601c" />
GO


SET DATEFORMAT YMD;


GO
IF (SELECT COUNT(*)
    FROM   [dbo].[EMP_Table]) = 0
    BEGIN
        PRINT (N'Add 3 rows to [dbo].[EMP_Table]');
        INSERT  INTO [dbo].[EMP_Table] ([ID], [emp_name], [dept_name], [devision], [location], [address], [pin], [address2], [address3], [add4])
        VALUES                        ('1         ', N'e1        ', N'd1        ', N'dev1      ', N'loc1      ', NULL, NULL, NULL, NULL, NULL);
        INSERT  INTO [dbo].[EMP_Table] ([ID], [emp_name], [dept_name], [devision], [location], [address], [pin], [address2], [address3], [add4])
        VALUES                        ('2         ', N'e2        ', N'd2        ', N'dev2      ', N'loc2      ', NULL, NULL, NULL, NULL, NULL);
        INSERT  INTO [dbo].[EMP_Table] ([ID], [emp_name], [dept_name], [devision], [location], [address], [pin], [address2], [address3], [add4])
        VALUES                        ('3', N'e2        ', N'd2        ', N'dev2      ', N'loc2      ', NULL, NULL, NULL, NULL, NULL);
    END


GO